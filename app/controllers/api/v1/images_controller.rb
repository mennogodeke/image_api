class Api::V1::ImagesController < Api::ApiController
	#repond_to :json

	def split_base64(attachment)
			  if attachment.match(%r{^data:(.*?);(.*?),(.*)$})
			    uri = Hash.new
			    uri[:type] = $1 # "image/gif"
			    uri[:encoder] = $2 # "base64"
			    uri[:data] = $3 # data string
			    uri[:extension] = $1.split('/')[1] # "gif"
			    return uri
			  else
			    return nil
			  end
	end

	def convert_data_uri_to_upload(obj_hash)
			  if obj_hash[:attachment].try(:match, %r{^data:(.*?);(.*?),(.*)$})
			    image_data = split_base64(obj_hash[:attachment])
			    image_data_string = image_data[:data]
			    image_data_binary = Base64.decode64(image_data_string)

			    temp_img_file = Tempfile.new("")
			    temp_img_file.binmode
			    temp_img_file << image_data_binary
			    temp_img_file.rewind

			    img_params = {:filename => "image.#{image_data[:extension]}", :type => image_data[:type], :tempfile => temp_img_file}
			    uploaded_file = ActionDispatch::Http::UploadedFile.new(img_params)

			    obj_hash[:attachment] = uploaded_file
			    obj_hash.delete(:image)
			  end
		return obj_hash    
	end

	def index
  		@images = Image.all
  	end

  	def new
  		@image = Image.new
  	end

	def create
	  	@image = Image.new(convert_data_uri_to_upload(image_params))
	  	if @image.save
	  		render status: 200, json:{
	  			message: "Uploaded with id" + @image.id.to_s,
				attachment: @image.attachment
			}
		else
			render status: 422, json:{
				errors: @user.errors
			}.to_json
		end
	end

	def destroy
	  	@image = Image.find(params[:id])
	  	@image.destroy 
	  	redirect_to root_path, notice: "Image Deleted!"
	end

	private
	  	def image_params
	  		params.require(:image).permit(:name, :attachment)
	  	end
end
